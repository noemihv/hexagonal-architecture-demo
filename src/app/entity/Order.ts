export type Order = {
    id: string;
    email: string;
    amount: number;
    comments: string;
    source: string;
}