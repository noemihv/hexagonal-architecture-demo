import { CreateOrderCommand } from "../command/CreateOrderCommand";
import { inject, injectable, named } from "inversify";
import { Order } from "../entity/Order";
import { OrdersInputPort } from "../port/OrdersInputPort";
import { OrderStoragePort } from "../port/OrderStoragePort";

@injectable()
export class OrderCreationService implements OrdersInputPort{
    constructor (
        //@inject ("Port") @named ("PostgresOrderStorage") private orderStorage: OrderStoragePort
        @inject ("Port") @named ("MongoOrderStorage") private orderStorage: OrderStoragePort
    ){}
    public async processOrder (command: CreateOrderCommand): Promise<void>{
        const order: Order = {
            id: command.id,
            email: command.customerEmail,
            amount: command.amount,
            source: command.source,
            comments: "Application Layer adding comment."
        }
        await this.orderStorage.saveOrder(order);
    }
}
