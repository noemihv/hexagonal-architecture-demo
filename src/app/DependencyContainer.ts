import { Container } from "inversify";
import { MongoClient } from "mongodb";
import { OrderCreationService } from "./service/OrderCreationService";
import { OrdersInputPort } from "./port/OrdersInputPort";
import { OrderStoragePort } from "./port/OrderStoragePort";
import { OrdersPostgresAdapter } from "../infra/OrdersPostgresAdapter";
import { OrdersMongoAdapter } from "../infra/OrdersMongoAdapter";
import { Pool } from "pg";

export class DependencyContainer extends Container {
    public boot(): void {
        this.bind<OrdersInputPort>('Port').to(OrderCreationService)
            .whenTargetNamed('OrderCreation');
        this.bind<Pool>('Client').toConstantValue(
            new Pool(
                {
                    host: 'super-e-commerce-postgres-db',
                    port: 5432,
                    user: 'super-e-commerce',
                    password: 'postgres',
                    database: 'postgres',
                    schema: 'super-e-commerce',
                    logging: false
                }
            )
        ).whenTargetNamed('Postgres');
        this.bind<OrderStoragePort>('Port').to(OrdersPostgresAdapter)
                .whenTargetNamed('PostgresOrderStorage');
        this.bind<MongoClient>('Client')
                .toConstantValue(new MongoClient('mongodb://root:example@mongo:27017/'))
                .whenTargetNamed('Mongo');
        this.bind<OrderStoragePort>('Port').to(OrdersMongoAdapter)
                .whenTargetNamed('MongoOrderStorage');
    }
}