export interface CreateOrderCommand {
    id: string;
    customerEmail: string;
    amount: number;
    source: string,
}
