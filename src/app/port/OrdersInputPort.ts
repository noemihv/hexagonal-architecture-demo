import { CreateOrderCommand } from "../command/CreateOrderCommand";

export interface OrdersInputPort {
    processOrder(command: CreateOrderCommand): Promise<void>;
}