import { Order } from "../entity/Order";

export interface OrderStoragePort {
    saveOrder (order: Order): Promise<void>;
}