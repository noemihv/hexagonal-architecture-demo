import { Order } from "../app/entity/Order";
import { OrderStoragePort } from "../app/port/OrderStoragePort";
import { PoolClient, Pool } from "pg";
import { inject, injectable, named } from "inversify";

@injectable ()
export class OrdersPostgresAdapter implements OrderStoragePort {
    constructor (
        @inject ("Client") @named ("Postgres") private postgresClient: Pool
    ){}

    public async saveOrder (order: Order): Promise<void>{
        const ordersClient: PoolClient = await this.createClientConnection();

        try {
            await ordersClient.query('BEGIN');

            await ordersClient.query(
                'WITH inserted as (INSERT INTO "super-e-commerce".orders (id,email,amount,comments, source) VALUES ($1, $2, $3, $4, $5) returning id, email, amount, comments, source) SELECT inserted.id, inserted.email, inserted.amount, inserted.comments, inserted.source FROM inserted',
                [order.id, order.email, order.amount, order.comments, order.source]
            );

            await ordersClient.query('COMMIT');
        } catch (error) {
            await ordersClient.query('ROLLBACK');
            throw new Error(error.message);

        } finally {
            ordersClient.release();
        }
    }

    private async createClientConnection(): Promise<PoolClient> {
        return await this.postgresClient.connect();
    }
}