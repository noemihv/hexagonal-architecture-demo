import { inject, injectable, named } from "inversify";
import { MongoClient } from "mongodb";
import { Order } from "../app/entity/Order";
import { OrderStoragePort } from "../app/port/OrderStoragePort";

@injectable ()
export class OrdersMongoAdapter implements OrderStoragePort {
    constructor(
        @inject ("Client") @named ("Mongo") private mongoClient: MongoClient
    ){}

    public async saveOrder (order: Order): Promise<void> {
        try {
            await this.mongoClient.connect();
            const db = this.mongoClient.db('super-e-commerce');
            const collection = db.collection('orders');

            await collection.insertOne({
                id: order.id,
                user: order.email,
                total: order.amount,
                comments: order.comments,
                source: order.source
            });

        } catch (error) {
            throw new Error(error.message);
;
        } finally {
            this.mongoClient.close();
        }
    }
}