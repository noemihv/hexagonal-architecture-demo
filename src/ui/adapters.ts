import 'reflect-metadata';
import {APIGatewayEvent, Handler, SQSEvent} from 'aws-lambda';
import { CreateOrderCommand } from '../app/command/CreateOrderCommand';
import { DependencyContainer } from '../app/DependencyContainer';
import { OrdersInputPort } from '../app/port/OrdersInputPort';

export const apiOrderCreationAdapter: Handler = async (event: APIGatewayEvent) => {
    const container = new DependencyContainer();
    container.boot();
    const orderCreationService: OrdersInputPort = container.getNamed("Port", "OrderCreation");
    const createOrderEvent = JSON.parse(event.body);
    const command: CreateOrderCommand = {
        id: createOrderEvent.id,
        customerEmail: createOrderEvent.email,
        amount: createOrderEvent.amount,
        source: createOrderEvent.source
    };
    await orderCreationService.processOrder(command);
    console.log(JSON.stringify(event));
    return {
        statusCode: 200,
        body: "APIGatewayEvent processed succesfully."
    }
};

export const queueOrderCreationAdapter: Handler = async (event: SQSEvent) => {
    const container = new DependencyContainer();
    container.boot();
    const orderCreationService: OrdersInputPort = container.getNamed("Port", "OrderCreation");
    const createOrderEvent = JSON.parse(event.Records[0].body);
    const command: CreateOrderCommand = {
        id: createOrderEvent.orderId,
        customerEmail: createOrderEvent.personalEmail,
        amount: createOrderEvent.amountPaid,
        source: createOrderEvent.source
    };
    await orderCreationService.processOrder(command);
    console.log(JSON.stringify(event));
};
