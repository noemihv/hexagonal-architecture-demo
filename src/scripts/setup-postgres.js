const { Pool } = require('pg');

async function setup() {
    const pool = new Pool({
        host: 'localhost',
        user: 'super-e-commerce',
        password: 'postgres',
        database: 'postgres',
        port: 5432
    });

    const client = await pool.connect();

    await client.query(`CREATE SCHEMA IF NOT EXISTS "super-e-commerce";`);
    await client.query(`CREATE SEQUENCE IF NOT EXISTS orders_id_seq;`);
    await client.query(`CREATE TABLE IF NOT EXISTS "super-e-commerce".orders (
        id varchar(36) NOT NULL PRIMARY KEY,
        email varchar(36) NOT NULL,
        amount real NOT NULL,
        source varchar(36) NOT NULL,
        comments varchar(36) NOT NULL
    );`);

    await client.end();
}

setup();